const { expect } = require("chai");

const {
  Network,
  Config,
  Hashgraph,
  SDK: { ContractFunctionParameters, AccountId, ContractId },
} = require("hashgraph-support");

describe("Testing a contract", function () {
  const destinationNetwork = Config.network;
  const client = Network.getNodeNetworkClient(destinationNetwork);
  const hashgraph = Hashgraph(client);

  const contractId = process.env.SCHEDULE_CONTRACT_ID;

  if (!contractId) {
    throw Error(
      "SCHEDULE_CONTRACT_ID: NOT FOUND IN ENV, deploy with 'make deploy-test CONTRACT=\"ContractName\"' to generate in ENV"
    );
  }

  it("should get points contract address", async () => {
  
    const response = await hashgraph.contract.query({
      contractId: contractId,
      method: "pointsContract"
    })
  
    const pointsContractId = ContractId.fromSolidityAddress(response.getAddress(0)).toString();
    expect(pointsContractId).to.equal(process.env.B4BPOINTS_CONTRACT_ID);
  })

  it("should call createTimeSlot", async () => {
    const time = Math.floor(new Date("2022-05-17").getTime() / 1000);
    console.log(time);

    const response = await hashgraph.contract.call({
      contractId: contractId,
      method: "createTimeSlot",
      params: new ContractFunctionParameters()
        .addUint256(time)
    })
  
    expect(response).to.be.true;
  })

  it("should call requestBooking", async () => {
    const time = Math.floor(new Date("2022-05-17").getTime() / 1000);
    const address = AccountId.fromString(process.env.HEDERA_ACCOUNT_ID).toSolidityAddress();

    const cost = 10**6;

    const response = await hashgraph.contract.call({
      contractId: contractId,
      method: "requestBooking",
      params: new ContractFunctionParameters()
        .addAddress(address)
        .addUint256(time)
        .addInt64(cost)
    })
  
    expect(response).to.be.true;
  })


  it("should call approveBooking", async () => {
    const time = Math.floor(new Date("2022-05-17").getTime() / 1000);
    const address = AccountId.fromString(process.env.HEDERA_ACCOUNT_ID).toSolidityAddress();
    // console.log(address);

    const response = await hashgraph.contract.call({
      contractId: contractId,
      method: "approveBooking",
      params: new ContractFunctionParameters()
        .addUint256(time)
        .addUint256(0)
    })
  
    expect(response).to.be.true;
  })
});
