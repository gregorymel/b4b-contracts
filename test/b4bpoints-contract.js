const { expect } = require("chai");

const {
  Network,
  Config,
  Hashgraph,
  SDK: { ContractFunctionParameters, AccountId },
} = require("hashgraph-support");

describe("Testing a contract", function () {
  const destinationNetwork = Config.network;
  const client = Network.getNodeNetworkClient(destinationNetwork);
  const hashgraph = Hashgraph(client);

  const contractId = process.env.B4BPOINTS_CONTRACT_ID;

  if (!contractId) {
    throw Error(
      "B4BPOINTS_CONTRACT_ID: NOT FOUND IN ENV, deploy with 'make deploy-test CONTRACT=\"ContractName\"' to generate in ENV"
    );
  }

  it("A contract will run a test", async () => {
    const address = AccountId.fromString(process.env.HEDERA_ACCOUNT_ID).toSolidityAddress();

    // Will run if a contract is Ownable
    const response = await hashgraph.contract.query({
      contractId: contractId,
      method: "balanceOf",
      params: new ContractFunctionParameters()
        .addAddress(address)
    });

    // const balance = response.getAddress(0);
    const balance = response.getUint256(0).toString();
    console.log(balance);

    // expect(accountId.toString()).to.equal(process.env.HEDERA_ACCOUNT_ID);
  });

});
