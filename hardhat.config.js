require("dotenv").config();
require("module-alias/register");
require("@nomiclabs/hardhat-waffle");

const shell = require("shelljs");
const { ContractId, AccountId, TokenId, AccountBalanceQuery } = require("@hashgraph/sdk");

const {
  Network,
  Config,
  Hashgraph,
  SDK: { ContractFunctionParameters, },
} = require("hashgraph-support");

/**
 * Leave this as a helper for deploying a smart contract,
 */
task("deploy", "Deploy a hedera contract")
  .addParam(
    "contract",
    "Name of contract that you wish to deploy to Hedera, from your /contracts folder"
  )
  .addOptionalParam(
    "destination",
    "The network that you are deploying to. Currently supporting previewnet/testnet",
    ""
  )
  .setAction(async (args) => {
    const destinationNetwork = args.destination || Config.network;
    const client = Network.getNodeNetworkClient(destinationNetwork);

    const contractInitialisation = {
      contractName: args.contract,
      // Optional, injected into the constructor, in this case for the "HelloWorld" Contract
      constructorParams: new ContractFunctionParameters()
        .addAddress(
          ContractId.fromString(process.env.B4BPOINTS_CONTRACT_ID).toSolidityAddress()
        )
        .addAddress(
          TokenId.fromString(process.env.USDC_TOKEN_ID).toSolidityAddress()
        )
    };

    const contractId = await Hashgraph(client).contract.create(
      contractInitialisation
    );

    // Check that contract test exist
    shell.exec(`bin/check-for-contract-test ${args.contract.toUpperCase()}`);

    // Inject the latest deployed contract ID into the env
    shell.exec(
      `bin/update-contract-id ${args.contract.toUpperCase()} ${contractId.toString()}`
    );

    console.log("Contract id: " + contractId.toString());
  });


task(
  "transfer-ownership",
  "Transfer ownership"
).setAction(async (args) => {
  const destinationNetwork = args.destination || Config.network;
  const client = Network.getNodeNetworkClient(destinationNetwork);
  const hashgraph = Hashgraph(client);
  const contractId = process.env.B4BPOINTS_CONTRACT_ID;

  const scheduleAddr = ContractId.fromString(process.env.SCHEDULE_CONTRACT_ID).toSolidityAddress();

  const transferOwnership = async (address) => {
    await hashgraph.contract.call({
      contractId: contractId,
      method: "transferOwnership",
      params: new ContractFunctionParameters()
        .addAddress(address)
    });
  };

  try {
    await transferOwnership(scheduleAddr);
  } catch (e) {
    console.warn(
      "If you are seeing this - ownership is already transfered"
    );
  }

  const result = await hashgraph.contract.query({
    contractId: contractId,
    method: "owner"
  });

  const owner = AccountId.fromSolidityAddress(result.getAddress(0)).toString();
  console.log(owner);
});

task(
  "balanceOf",
  "acount balance"
).setAction(async (args) => {
  const destinationNetwork = args.destination || Config.network;
  const client = Network.getNodeNetworkClient(destinationNetwork);

  const query = new AccountBalanceQuery()
    .setAccountId(process.env.HEDERA_ACCOUNT_ID);

  const tokenBalance = await query.execute(client);
  console.log("The token balance(s) for this account: " + tokenBalance.tokens.toString());
})

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: "0.8.0",
  // NOTE: Adding the optimiser by default, may remove later
  optimizer: {
    enabled: true,
    runs: 1000,
  },
};
