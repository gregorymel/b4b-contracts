const {
    FileCreateTransaction,
    FileAppendTransaction,
    ContractCreateTransaction,
    ContractExecuteTransaction,
    ContractCallQuery,
    Hbar,
    Client,
    PrivateKey,
} = require("@hashgraph/sdk");

const accountId = process.env.HEDERA_ACCOUNT_ID;
const privateKey = process.env.HEDERA_PRIVATE_KEY;

const client = Client
    .forTestnet()
    .setOperator(accountId, privateKey)
    .setMaxTransactionFee(new Hbar(10));

const QueryContract = async (client, { contractId, method, params }) => {
    const contractCallResult = await new ContractCallQuery()
        .setContractId(contractId)
        .setGas(GAS_CONTRACT)
        .setQueryPayment(new Hbar(10))
        .setFunction(method, params)
        .execute(client);
    
    if (contractCallResult.errorMessage) {
        throw `error calling contract: ${contractCallResult.errorMessage}`;
    }
    
    return contractCallResult;
};
    
const CallContract = async (client, { contractId, method, params = null }) => {
    const contractTransaction = await new ContractExecuteTransaction()
        .setContractId(contractId)
        .setGas(GAS_CONTRACT)
        .setFunction(method, params)
        .freezeWith(client);

    const contractTransactionResponse = await contractTransaction.execute(client);

    const contractReceipt = await contractTransactionResponse.getReceipt(client);

    return contractReceipt.status.toString() === "SUCCESS";
};

const Hashgraph = (client) => ({
    contract: {
        call: (params) => CallContract(client, params),
        query: (params) => QueryContract(client, params),
        sub: (params) => SubscribeToEmittedEvents(client, params),
    }
});

const hashgraph = Hashgraph(client);
const contractId = process.env.SCHEDULE_CONTRACT_ID;
const pointsContractId = process.env.B4BPOINTS_CONTRACT_ID;

const time = Math.floor(new Date("2022-05-17").getTime() / 1000);
const address = AccountId.fromString(process.env.HEDERA_ACCOUNT_ID).toSolidityAddress();

// creatTimeSlot
const response1 = await hashgraph.contract.call({
    contractId: contractId,
    method: "createTimeSlot",
    params: new ContractFunctionParameters()
        .addUint256(time)
})

// requestBooking
const cost = 10**6;
const response2 = await hashgraph.contract.call({
    contractId: contractId,
    method: "requestBooking",
    params: new ContractFunctionParameters()
        .addAddress(address)
        .addUint256(time)
        .addInt64(cost)
})

// approveBooking
const response3 = await hashgraph.contract.call({
    contractId: contractId,
    method: "approveBooking",
    params: new ContractFunctionParameters()
        .addUint256(time)
        .addUint256(0)
})


// Will run if a contract is Ownable
const result = await hashgraph.contract.query({
    contractId: pointsContractId,
    method: "balanceOf",
    params: new ContractFunctionParameters()
      .addAddress(address)
  });