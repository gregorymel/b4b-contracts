/// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";

import "./libraries/hashgraph/HederaTokenService.sol";
import "./libraries/hashgraph/HederaResponseCodes.sol";
import "./B4BPoints.sol";

contract Schedule is HederaTokenService {
    struct AdInfo {
        uint256 lastTimeBooked;
        address bookedBy;
        address[] requests;
        int64 cost;
    }

    /* ========== STATE VARIABLES ========== */
    mapping(address => mapping(uint256 => AdInfo)) public timeSlots;


    uint256 private length;
    IB4BPoints public pointsContract;
    address public usdcAddress;

    // constants
    uint256 private DEFAULT_REWARD = 10;
    int64 private DEFAULT_COST = 10;

    /* ========== CONSTRUCTOR ========== */

    constructor(address _pointsContract, address _usdcAddress) {
        pointsContract = IB4BPoints(_pointsContract);
        usdcAddress = _usdcAddress;

        HederaTokenService.associateToken(address(this), usdcAddress);
    }

    /* ========== MUTATIVE FUNCTIONS ========== */

    function createTimeSlot(uint256 time) public {
        require(time >= block.timestamp, "Invalid time");

        address[] memory requests; 
        AdInfo memory ad = AdInfo(0, address(0), requests, DEFAULT_COST);
        timeSlots[msg.sender][time] = ad;
    }

    // function deleteTimeSlot(uint256 slotId) public {

    // }

    function requestBooking(address userAddr, uint256 slotId, int64 cost) public {
        timeSlots[userAddr][slotId].lastTimeBooked = block.timestamp;
        timeSlots[userAddr][slotId].requests.push(msg.sender);
        timeSlots[userAddr][slotId].cost = cost;

        int response = HederaTokenService.transferToken(usdcAddress, msg.sender, address(this), cost);

        if (response != HederaResponseCodes.SUCCESS) {
            revert("Transfer Failed");
        }
    }

    // function cancelBooking(uint256 slotId) public {

    // }

    function approveBooking(uint256 slotId, uint256 requestNum) public {
        address[] memory requests = timeSlots[msg.sender][slotId].requests;
        require(requests.length > requestNum, "Invalid request nummber");

        timeSlots[msg.sender][slotId].bookedBy = requests[requestNum];

        pointsContract.addPoints(msg.sender, DEFAULT_REWARD);

        int64 cost = timeSlots[msg.sender][slotId].cost;
        int response = HederaTokenService.transferToken(usdcAddress, address(this), msg.sender, cost);

        if (response != HederaResponseCodes.SUCCESS) {
            revert("Transfer Failed");
        }
    }

    // function complete
}