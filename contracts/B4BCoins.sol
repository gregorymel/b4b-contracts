//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract B4BCoins is ERC20 {
    constructor(uint256 initialSupply) ERC20("B4BCoins", "B4BC") {
        _mint(msg.sender, initialSupply);
    }
}