/// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";

contract B4BPoints is Ownable {
    mapping(address => uint256) public points;

    constructor() Ownable() {}

    function addPoints(address userAddr, uint256 amount) external onlyOwner {
        points[userAddr] += amount;
    }

    function balanceOf(address userAddr) public view returns(uint256) {
        return points[userAddr];
    }
}

interface IB4BPoints {
    function addPoints(address userAddr, uint256 amount) external;
}